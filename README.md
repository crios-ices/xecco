# xECCO # 
This is the CRIOS/ECCO users' port of the 
[gcmfaces Matlab toolbox](https://github.com/gaelforget/gcmfaces)
to Python. We aim not only to port all existing functionality, so that
xECCO will serve as a drop-in replacement for current gcmfaces users,
but to cover an even larger set of post-processing needs and to improve
usability along the way.

## Design Objectives ##
xECCO should:

  - Make use of robust existing libraries where possible;
  - "Just work" for any existing MITgcm configuration (working for ECCO v4
    and for the ASTE configuration are first priorities);
  - Be usable for any data set that you can fit on disk; this may be achieved
    using `dask` for out-of-core array data structures as in xgcm. This may 
    even be easily extensible to analysis on distributed memory machines, as 
    data sets become too large for the average workstation.
  - Be well documented and easy to understand; compatibility with Jupyter
    notebooks may make this much easier to achieve than with Matlab scripts.

## Contribution guidelines ##
  - *up for discussion* I think we should base the project in Python 3; 
    Python 2 will be cease to be supported at some point whereas 3 should
    continue to be backward-compatible indefinitely, hopefully. This may
    be problematic as systems that aren't kept up to date have Python
    distributions that are too old to work correctly with necessary libraries
    (case in point, the python3 on ICES desktops is too old to support numpy).
    Fortunately building Python yourself is straightforward.
  - The first implementation of any functionality should be tested against
    existing gcmfaces routines. We should come up with a few standard test
    cases.
  - (Once there is a working base) Regression test any changes against
    the last confirmed working xECCO version.

## Resources ##
See Gael Forget's repository for the definitive gcmfaces version. 
[This repository](https://bitbucket.org/crios-ices/crios-faces) contains An Nguyen's 
modifications to gcmfaces for the ASTE project.

## License ##
Discuss. MIT license is probably good, and compatible with xmitgcm.
